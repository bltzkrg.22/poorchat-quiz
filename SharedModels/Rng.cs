﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedModels
{
    public static class Rng
    {
        private static Random rng = new Random();

        public static int GetRandom() => rng.Next();

        /// <summary>
        /// Generates a random number in range from 0 to max - 1, inclusive.
        /// </summary>
        public static int GetRandom(int max) => rng.Next(max);

        /// <summary>
        /// Generates a random number in range from min to max - 1, inclusive.
        /// </summary>
        public static int GetRandom(int min, int max) => rng.Next(min, max);

        /// <summary>
        /// Shuffles values stored in a list/array using Knuth shuffle (i.e. Fisher-Yates shuffle) algorithm.
        /// </summary>
        public static void KnuthShuffle<T>(IList<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int j = Rng.GetRandom(i, list.Count);
                T swapBuffer = list[i];
                list[i] = list[j];
                list[j] = swapBuffer;
            }
        }

        
        /// <summary>
        /// Shuffles only the values stored in a dictionary. After shuffling each key will be mapped
        /// to a random, previously existing value.
        /// </summary>
        public static void DictShuffle<T, U>(Dictionary<T, U> dict) where T : struct where U : struct
        {
            int n = dict.Count;

            T[] keyArray = dict.Keys.ToArray();
            KnuthShuffle(keyArray);

            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                U value = dict[keyArray[k]];
                dict[keyArray[k]] = dict[keyArray[n]];
                dict[keyArray[n]] = value;
            }
        }

    }
}
