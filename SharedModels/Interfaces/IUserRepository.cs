﻿namespace SharedModels.Interfaces
{
    public interface IUserRepository
    {
        public string GetUserColor(string nickname);
    }
}
