﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharedModels.Models;
using SharedModels.Interfaces;
using SharedModels;

namespace DataAccessLibrary
{
    public class QuestionsSqlRepository : IQuestionRepository
    {
        private readonly ISqlDataAccess _dbase;
        public List<uint> ShuffledQuestionsIds { get; private set; } = null;
        public int CurrentShuffledIndex { get; private set; } = 0;
        public int Count => ShuffledQuestionsIds.Count;

        public QuestionsSqlRepository(ISqlDataAccess dbase)
        {
            _dbase = dbase;

            ShuffledQuestionsIds = GetQuestionIds();
            Rng.KnuthShuffle(ShuffledQuestionsIds);
        }




        public List<uint> GetQuestionIds()
        {
            string sqlQuery = "SELECT Id FROM dbo.Questions";

            return _dbase.LoadData<uint, dynamic>(sqlQuery, new { });
        }

        public Question GetQuestion(uint id)
        {
            string sqlQuery = $"SELECT * FROM dbo.Questions WHERE Id = {id}";

            return _dbase.LoadSingle<Question, dynamic>(sqlQuery, new { });
        }

        public Question GetRandomQuestion()
        {
            CurrentShuffledIndex++;
            return GetQuestion(ShuffledQuestionsIds[CurrentShuffledIndex - 1]);
        }
    }
}
