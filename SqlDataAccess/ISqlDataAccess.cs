﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    public interface ISqlDataAccess
    {
        string ConnectionStringName { get; set; }

        List<T> LoadData<T, U>(string sql, U parameters);
        T LoadSingle<T, U>(string sql, U parameters);
        void SaveData<T>(string sql, T parameters);
    }
}